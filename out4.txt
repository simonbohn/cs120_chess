1. Start a new game
2. Load a saved game
Enter a file to load from:
Black turn 4:
[34mR [34mN [34mB [34mQ [34mK [34mB [34mN [34mR 
[34mP [34mP [34mP [34mP [39m* [34mP [34mP [34mP 
[39m* [39m* [39m* [39m* [39m* [39m* [39m* [39m* 
[39m* [39m* [39m* [39m* [34mP [39m* [39m* [39m* 
[39m* [39m* [39m* [39m* [39m* [39m* [33mP [39m* 
[39m* [39m* [39m* [39m* [39m* [33mP [39m* [39m* 
[33mP [33mP [33mP [33mP [33mP [39m* [39m* [33mP 
[33mR [33mN [33mB [33mQ [33mK [33mB [33mN [33mR 
[39mBlack turn 4:
Black checks!
Black checkmates!!!
Game over. Goodbye!
[34mR [34mN [34mB [39m* [34mK [34mB [34mN [34mR 
[34mP [34mP [34mP [34mP [39m* [34mP [34mP [34mP 
[39m* [39m* [39m* [39m* [39m* [39m* [39m* [39m* 
[39m* [39m* [39m* [39m* [34mP [39m* [39m* [39m* 
[39m* [39m* [39m* [39m* [39m* [39m* [33mP [34mQ 
[39m* [39m* [39m* [39m* [39m* [33mP [39m* [39m* 
[33mP [33mP [33mP [33mP [33mP [39m* [39m* [33mP 
[33mR [33mN [33mB [33mQ [33mK [33mB [33mN [33mR 
[39m