#include <assert.h>
#include <cctype>
#include <iostream>
#include <cstdio>
#include "Game.h"
#include "Prompts.h"
#include "Chess.h" 

///////////////
// Board //
///////////////



void Board::run(){


        string userInput;
        char startColRaw;
        int startRowRaw;
        char endColRaw;
        int endRowRaw;
        int n;
	    int gameOn = 1;
        int printBoardVar = 0;
        int moveMade = 0;
        string savegameName;
        while(gameOn == 1){ 
            Prompts::playerPrompt(playerTurn(), turn());
            moveMade = 0;
            std::getline(cin,userInput);
            n = std::sscanf(userInput.c_str(),"%1c%d %c%d",&startColRaw,&startRowRaw,&endColRaw,&endRowRaw);
            if(userInput == "q" || userInput == "Q"){
                    gameOn = 1;
                    break;
            }
            else{
                    if(userInput == "board"){
                            ++printBoardVar;

                    }
                    else if(userInput == "save"){
                            Prompts::saveGame();
                            std::getline(cin,savegameName);
                            saveGame(savegameName);
                    }
                    else if(userInput == "forfeit"){
                            if(playerTurn() == WHITE){
                                Prompts::win(BLACK,turn());
                            }
                            else{
                                Prompts::win(WHITE,turn());
                            }
                            break;
                    }
                    else if(n == 4){  
                            Position startPos(letterToNumber(startColRaw),chessNumToIndex(startRowRaw));
                            Position endPos(letterToNumber(endColRaw),chessNumToIndex(endRowRaw));
    //                        int stale = checkStale(playerTurn()); // check this before doing any move
	//		    if (stale == 1) { // stalemate is totally a current-board thing
	//		        gameOn = 0; // so no need to make any type of tentative move.
	//		    }
                            moveMade = makeMove(startPos, endPos); // make the move
			    if(moveMade == SUCCESS){
                                 if (checkCheck(playerTurn()) == 1) { // if in check
                                    Prompts::check(playerTurn());
			             if (checkCheckmate(playerTurn()) == 1) { // if in checkmate
				         //Prompts::checkMate(playerTurn());
				         Prompts::gameOver();
					 cout << std::endl;
					 gameOn = 0; // end the game
				     }
                                }    
				++m_turn;
                            }
                            pawnPromotion(); // looks for pawns in the top and bottom ranks
			    // this can only happen if they moved there from the other side
			    // therefore they can promote. We automatically promote to queen
			    // as per the assignement guidelines.
                    }
                    else{
                            Prompts::parseError();
                    }
            }
            if(printBoardVar%2 == 1){
                printBoard();
            }
        }
}

int Board::makeMove(Position start, Position end) {
    unsigned int start_index = index(start);
    unsigned int end_index = index(end);
    Piece * temp = getPiece(start);
    if (temp == nullptr) {
        Prompts::noPiece();
	return MOVE_ERROR_ILLEGAL;
    }

    int Valid = temp->validMove(start, end, *this);
    int ValidCheck = validMoveCheck(start, end);
    if (Valid > 0) { // if it's a valid move accoring to the type of piece
	if (ValidCheck > 0) { // if not moving into check    
            delete(getPiece(end));
            m_pieces[end_index] = m_pieces[start_index];
            m_pieces[start_index] = nullptr;
	    if (Valid == MOVE_CAPTURE) {
		    Prompts::capture(playerTurn());
	    } 
	    return SUCCESS;
        }
        else {
	    if (ValidCheck == MOVE_ERROR_CANT_EXPOSE_CHECK) {
	        Prompts::cantExposeCheck();
	    }
	}	
    }
    else {
	 if (Valid == MOVE_ERROR_BLOCKED) {
             Prompts::blocked();
         }
         else if (Valid == MOVE_ERROR_ILLEGAL) {
             Prompts::illegalMove();
         }
         else if(Valid == MOVE_ERROR_OUT_OF_BOUNDS){
             Prompts::outOfBounds();
         }
        else if(Valid == MOVE_ERROR_NO_PIECE){
            Prompts::noPiece();
        }
    }
    return 0;
}

int Board::validMoveCheck(Position start, Position end/*, Board& board*/) {
    unsigned int start_index = index(start);
    unsigned int end_index = index(end);
    Player OWNER;
    if (playerTurn() == WHITE) {
        OWNER = WHITE;
    }
    else {
        OWNER = BLACK;
    }
    Player ENEMY;
    if (OWNER == WHITE) {
        ENEMY = BLACK;
    }
    else {
        ENEMY = WHITE;
    }
    ++m_turn; // change who the player is because we need to know if your enemy is checking you, not if you are checking the enemy, and this would be taking place in the next turn.
    Piece * TEMP = getPiece(end);
    delete(getPiece(end));
    m_pieces[end_index] = m_pieces[start_index];
    m_pieces[start_index] = nullptr; // these lines above make the move, but only temporarily
    if (checkCheck(ENEMY) == 1) {
        m_pieces[start_index] = m_pieces[end_index];
        m_pieces[end_index] = nullptr;
	if (TEMP != nullptr) {
            initPiece(TEMP->id(), playerTurn(), end); // this undoes the move if in check
	}
	--m_turn; //go back to proper move
	return MOVE_ERROR_CANT_EXPOSE_CHECK;	
    }
        // undoes the temp move if not in check
        m_pieces[start_index] = m_pieces[end_index];
        m_pieces[end_index] = nullptr;
	if (TEMP != nullptr) {
            initPiece(TEMP->id(), playerTurn(), end);
	}
	--m_turn; // go back to proper move if not in check
    return SUCCESS;    
}

int Board::checkCheck(Player player) {
    int King_x = 0;
    int King_y = 0;
    Player ENEMY;
    if (player == BLACK) {
        ENEMY = WHITE;
    }
    else if (player == WHITE) {
        ENEMY = BLACK;
    }
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) { 
	    Piece * temp = getPiece(Position(i,j));
	    if ((temp != nullptr) && (temp->owner() == ENEMY) && (temp->id() == 5)) {//find player's king
	        King_x = i;
	        King_y = j;
	        break;
	    }
	}
    }
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
	    Piece * temp = getPiece(Position(i,j));
	    if ((temp != nullptr) && (temp->owner() == player)) {
	        if (temp->validMove(Position(i,j),Position(King_x,King_y),*this) > 0) { // check if each piece has valid move to location of king
			return 1; // in check
		}
	    }
	}
    }
    return 0; // not in check by default
}

int Board::checkCheckmate(Player player) { // brings in the player who's turn it is currently
    // this function will assume that the enemy is already in check, i.e. player is checking enemy
    Player ENEMY;
    if (player == BLACK) {
        ENEMY = WHITE;
    }
    else if (player == WHITE) {
        ENEMY = BLACK;
    }
    ++m_turn;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
	    Piece * temp = getPiece(Position(i,j));
	    if ((temp != nullptr) && (temp->owner() == ENEMY)) { // find enemy pieces
	        for (int k = 0; k < 8; k++) {
		    for (int l = 0; l < 8; l++) {
			Piece * TEMP = getPiece(Position(k,l));
		        if (temp->validMove(Position(i,j),Position(k,l),*this) > 0) { // if valid move
			    unsigned int start_index = index(Position(i,j));
                            unsigned int end_index = index(Position(k,l));
			    delete(getPiece(Position(k,l)));
    			    m_pieces[end_index] = m_pieces[start_index];
    			    m_pieces[start_index] = nullptr;
			    --m_turn;
			    if (checkCheck(player) == 0) {//if tentative move makes player no longer check enemy
				// the following undoes the tentative move
				m_pieces[start_index] = m_pieces[end_index];
        			m_pieces[end_index] = nullptr;
        			if (TEMP != nullptr) {
            			initPiece(TEMP->id(), playerTurn(), Position(k,l));
        			}
			    return 0; // not in mate
			    }
			    ++m_turn;
			    // the following undoes the move in the case where
			    // the move didn't bring enemy out of check
			    m_pieces[start_index] = m_pieces[end_index];
    			    m_pieces[end_index] = nullptr;
    			    if (TEMP != nullptr) {
        		        initPiece(TEMP->id(), playerTurn(), Position(k,l));
    			    }
			}
		    }
		}
	    }
	}
    } 
    --m_turn;
    Prompts::checkMate(playerTurn());
    return 1; // in mate
}

int Board::checkStale(Player player) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
	    Piece * temp = getPiece(Position(i,j)); // get your pieces
	    if ((temp != nullptr) && (temp->owner() == player)) {
	        for (int k = 0; k < 8; k++) {
		    for (int l = 0; l < 8; l++) {
		        if (temp->validMove(Position(i,j),Position(k,l),*this) > 0) { // find a valid move
                if(validMoveCheck(Position(i,j),Position(k,l)) ==  1) {

	//		    cout<<i<<j<<k<<l;
                    return 0;
                }

			}
		    }
		}
	    }
	}
    }    
    Prompts::staleMate();
    Prompts::gameOver();
    cout << std::endl;
    return 1; // if no valid move exists, game over and stalemate
}

void Board::saveGame(string outName){
        //To get rid of the warning
        cout<<outName<<std::endl<<"If that printed, the inheritance isn't working correctly";
}

int Board::loadGame(string inName){
        cout<<inName<<std::endl<<"If that printed, the inheritance isn't working correctly";
        return 1;
}



//This function converts a letter of board space to a number of board space. For example, a is 0, b is 21 etc.. These will  used for generating position pairs.
int Board::letterToNumber(char letter){
    if(letter == 'a' || letter == 'A'){
        return 0;
    }
    else if(letter == 'b' || letter == 'B'){
        return 1;
    }
    else if(letter == 'c' || letter == 'C'){
        return 2;
    }
    else if(letter == 'd' || letter == 'D'){
        return 3;
    }
    else if(letter == 'e' || letter == 'E'){
        return 4;
    }
    else if(letter == 'f' || letter == 'F'){
        return 5;
    }
    else if(letter == 'g' || letter == 'G'){
        return 6;
    }
    else if(letter == 'h' || letter == 'H'){
        return 7;
    }
    else{
        return 100;//If input is not one of the acceptable letters, return -1
    }
    return 100; 
}

//Converts chess numbers (1,2,3 etc) to the index we use (0,1,2 etc...) as well as returning -1 for invalid numbers
int Board::chessNumToIndex(int num){
    if(num > 0 && num < 9){
        --num;
        return num;
    }
    else{
        return -1;
    }
}

void Board::pawnPromotion(){
}

Board::~Board() {
    // Delete all pointer-based resources
    for (unsigned int i=0; i < m_width * m_height; i++)
        delete m_pieces[i];
    for (size_t i=0; i < m_registeredFactories.size(); i++)
        delete m_registeredFactories[i];
}

// Get the Piece at a specific Position, or nullptr if there is no
// Piece there or if out of bounds.
Piece* Board::getPiece(Position position) const {
    if (validPosition(position))
        return m_pieces[index(position)];
    else {
        Prompts::outOfBounds();
	//cout << "getPiece\n";
        return nullptr;
    }
}

// Create a piece on the board using the factory.
// Returns true if the piece was successfully placed on the board
bool Board::initPiece(int id, Player owner, Position position) {
    Piece* piece = newPiece(id, owner);
    if (!piece) return false;

    // Fail if the position is out of bounds
    if (!validPosition(position)) {
        Prompts::outOfBounds();
        cout << "initPiece\n";
	return false;
    }
    // Fail if the position is occupied
    if (getPiece(position)) {
        Prompts::blocked();
        return false;
    }
    m_pieces[index(position)] = piece;
    return true;
}

// Add a factory to the Board to enable producing
// a certain type of piece
bool Board::addFactory(AbstractPieceFactory* pGen) {
    // Temporary piece to get the ID
    Piece* p = pGen->newPiece(WHITE);
    int id = p->id();
    delete p;

    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        m_registeredFactories[id] = pGen;
        return true;
    } else {
        std::cout << "Id " << id << " already has a generator\n";
        return false;
    }
}

void Board::printBoard(){
}


// Search the factories to find a factory that can translate `id' to
// a Piece, and use it to create the Piece. Returns nullptr if not found.
Piece* Board::newPiece(int id, Player owner) {
    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        std::cout << "Id " << id << " has no generator\n";
        return nullptr;
    }// else {
        return it->second->newPiece(owner);
   // }
}

