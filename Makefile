CXX = g++
CXXFLAGS = -std=c++11 -Wextra -Wall -pedantic -g

chess: Chess.o Board.o Piece.o ChessGame.o
	$(CXX) $(CXXFLAGS) Chess.o Board.o ChessGame.o Piece.o -o chess

unittest: unittest.o Board.o Piece.o ChessGame.o
	$(CXX) $(CXXFLAGS) unittest.o Board.o ChessGame.o Piece.o -o unittest

Board.o: Game.h
Chess.o: Game.h Chess.h Prompts.h ChessGame.h
Game.o:  Game.h Chess.h ChessGame.h
Piece.o: Chess.h Game.h ChessGame.h
ChessGame.o: Chess.h Piece.h ChessGame.h
unittest.o: unittest.h Piece.h ChessGame.h Game.h Prompts.h Terminal.h

clean:
	rm *.o chess

