
#undef FOR_RELEASE
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"
#include "Piece.h"

using std::cout;

int Piece::validMove(Position start, Position end, const Board& board) const{
        //cout<<"valid move is being inherited properly";

        //I was thinking we could put the checks that stops it from moving off the board, and the one that stops it from moving into your own piece and anything else you can think of here. I don't want to go digging for those in your code though.


    //Unsigned ints roll around to a very large positive number so we don't check for less than zero
    if(start.x > 7 || start.y > 7 || end.x > 7 || end.y > 7){
            return MOVE_ERROR_OUT_OF_BOUNDS;
    }
    

    int OWNER = board.playerTurn();

    Piece * temp = board.getPiece(start); // look at piece at start pos
    Piece * TEMP = board.getPiece(end); // look at piece at end pos

    if ((temp != nullptr) && (temp->owner() != OWNER)) { // if player tries to move enemy piece
	//cout << "ENEMY PIECE CANT BE MOVED\n";
	return MOVE_ERROR_NO_PIECE;
    }
    if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {//if player tries moving onto own piece
        return MOVE_ERROR_BLOCKED;
    }

    return SUCCESS;
}

int Piece::validStraight(Position start, Position end, const Board& board) const {
    Piece * temp = board.getPiece(start);
    int dy = end.y - start.y;
    int dx = end.x - start.x;
    int OWNER = temp->owner();
    int ENEMY;
    if (OWNER == 1) {
        ENEMY = 0;
    }
    else {
        ENEMY = 1;
    }
    if ((dy != 0) && (dx != 0)) {
        return MOVE_ERROR_ILLEGAL;
    }
        if (dy == 0) { // y constant so x changing, so **HORIZONTAL**
            if (dx > 0) {
                    // RIGHTWARD
                for (unsigned int i = start.x + 1; i <= end.x; i++) { // iter to endpoint
                    Piece * temp11 = board.getPiece(Position(i, start.y));
                    if (temp11 != nullptr) { // run into piece
                        if ((temp11->owner() == ENEMY) && (i == end.x)) {
                            return MOVE_CAPTURE;
                        }
                        return MOVE_ERROR_BLOCKED;
                   }
                }
            }
                else if (dx < 0) {
                // LEFTWARD
                    for (unsigned int k = start.x - 1; k >= end.x; k--) { // iter to endpoint
                        Piece * temp12 = board.getPiece(Position(k, start.y));
                        if (temp12 != nullptr) { // run into piece
                            if ((temp12->owner() == ENEMY) && (k == end.x)) {
                                return MOVE_CAPTURE;
                            }
                            return MOVE_ERROR_BLOCKED;
                        }
                    }
                }
            }
            else if (dx == 0) { // x constant so y changing, so **VERTICAL**
                if (dy > 0) {
                        // UPWARD
                    for (unsigned int j = start.y + 1; j <= end.y; j++) {//iter to endpoint
                        Piece * temp13 = board.getPiece(Position(start.x, j));
                        if (temp13 != nullptr) {//run into piece
                            if ((temp13->owner() == ENEMY) && (j == end.y)) {
                                return MOVE_CAPTURE;
                            }
                            return MOVE_ERROR_BLOCKED;
                        }
                    }
                }
                else if (dy < 0){
                        // DOWNWARD
                    for (unsigned int n = start.y - 1; n >= end.y; n--) {
                        Piece * temp14 = board.getPiece(Position(start.x, n));
                        if (temp14 != nullptr) {//run into own piece
                            if ((temp14->owner() == ENEMY) && (n == end.y)) {
                                 return MOVE_CAPTURE;
                            }
                            return MOVE_ERROR_BLOCKED;
                        }
                    }
                }
            }
    return SUCCESS;
}

int Piece::validDiag(Position start, Position end, const Board& board) const{
    int dx = end.x - start.x;
    int dy = end.y - start.y;
    int DX = sqrt(dx*dx);
    int DY = sqrt(dy*dy);

    Piece * temp = board.getPiece(start);

    int OWNER = temp->owner();
    int ENEMY;
    if (OWNER == 1) {
        ENEMY = 0;
    }
    else {
        ENEMY = 1;
    }
    if (DX != DY) {
        return MOVE_ERROR_ILLEGAL;
    }
        // North-East
        if ((dy > 0) && (dx > 0)) {
            unsigned int i = start.x + 1;
            unsigned int j = start.y + 1;
            while ((i != end.x) && (j != end.y)) { //iter to endpoint
                Piece * temp11 = board.getPiece(Position(i,j));
                if (temp11 != nullptr) {//run into  piece
                    if ((temp11->owner() == ENEMY) && (i == end.x) && (j == end.y)) {
                        return MOVE_CAPTURE;
                    }
                    return MOVE_ERROR_BLOCKED;
                }
                i++;
                j++;
            }
        }
        // South-East
        else if ((dy < 0) && (dx > 0)) {
            unsigned int m = start.x + 1;
            unsigned int n = start.y - 1;
            while ((m != end.x) && ( n != end.y)) {
                Piece * temp12 = board.getPiece(Position(m,n));
                if (temp12 != nullptr) {//run into piece
                    if ((temp12->owner() == ENEMY) && (m == end.x) && (n == end.y)) {
                        return MOVE_CAPTURE;
                    }
                    return MOVE_ERROR_BLOCKED;
                }
                m++;
                n--;
            }
        }
	  // South-West
        else if ((dy < 0) && (dx < 0)) {
            unsigned int p = start.x - 1;
            unsigned int q = start.y - 1;
            while ((p != end.x) && ( q != end.y)) {
                Piece * temp13 = board.getPiece(Position(p,q));
                if (temp13 != nullptr) {//run into piece
                    if ((temp13->owner() == ENEMY) && (p == end.x) && (q == end.y)) {
                        return MOVE_CAPTURE;
                    }
                    return MOVE_ERROR_BLOCKED;
                }
                p--;
                q--;
            }
        }
        // North-West
        else if ((dy > 0) && (dx < 0)) {
            unsigned int t = start.x - 1;
            unsigned int s = start.y + 1;
            while ((t != end.x) && (s != end.y)) {
                Piece * temp14 = board.getPiece(Position(t,s));
                if (temp14 != nullptr) {//run into piece
                    if ((temp14->owner() == ENEMY) && (t == end.x) && (s == end.y)) {
                        return MOVE_CAPTURE;
                    }
                    return MOVE_ERROR_BLOCKED;
                }
                t--;
                s++;
            }
        }
    return SUCCESS;
}

int Pawn::validMove(Position start, Position end, const Board& board) const {
    int b = Piece::validMove(start,end,board);
    if (b != SUCCESS) {
	return b;
    }
    Piece * temp = board.getPiece(start); //look at piece at start position
    Piece * TEMP = board.getPiece(end); // look at piece at end position
    
    int dy = end.y - start.y;
    int dx = end.x - start.x;
    int DX = sqrt(dx*dx);
    int DY = sqrt(dy*dy);
    int OWNER = temp->owner();
    int ENEMY;
    if (OWNER == 1) {
        ENEMY = 0;
    }
    else {
        ENEMY = 1;
    }
    if (temp->owner() == WHITE) {
        if (dy < 0) { // White pawn can only move UP
		return MOVE_ERROR_ILLEGAL;
	}
        if (start.y == 1) {  // if in starting rank
	    if (DY > 2) {
	        return MOVE_ERROR_ILLEGAL;
	    }
	    for (unsigned int i = start.y + 1; i <= end.y; i++) {
	        Piece * temp11 = board.getPiece(Position(start.x,i));
	        if (temp11 != nullptr) {
	            return MOVE_ERROR_BLOCKED;
	        }
	    }
	} 
	else {
            if (DY > 1) {
	        return MOVE_ERROR_ILLEGAL;
	    }
	    if ((DX == 0) && (TEMP != nullptr)) {
                return MOVE_ERROR_BLOCKED;
            }
	}
	 if (DX != DY) {
                if (DX != 0) {
                    return MOVE_ERROR_ILLEGAL;
                }
	 }
	if (DX == DY) {
	    if ((DX != 1) && (DY != 1)) {
		return MOVE_ERROR_ILLEGAL;
	    } 
	    if ((DX == 1) && (DY == 1) && TEMP == nullptr) {
		return MOVE_ERROR_ILLEGAL;
	    }
	    if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
		return MOVE_CAPTURE;
	    }
	}	
    }
    else if (temp->owner() == BLACK) {
        if (dy > 0) { // Black pawn can only move DOWN
	    return MOVE_ERROR_ILLEGAL;
	}
	if (start.y == 6) { // if in starting rank
	    if (DY > 2) {
	        return MOVE_ERROR_ILLEGAL;
            }
	    for (unsigned int i = start.y - 1; i >= end.y; i--) {
                Piece * temp11 = board.getPiece(Position(start.x,i));
                if (temp11 != nullptr) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
	}
	else {
	    if (DY > 1) {
	        return MOVE_ERROR_ILLEGAL;
	    }
	    if ((DX ==0) && (TEMP != nullptr)) {
                   return MOVE_ERROR_BLOCKED;
            }
        }
	if (DX != DY) {
	    if (DX != 0) {
	        return MOVE_ERROR_ILLEGAL;
	    }
	}
	if (DX == DY) {
            if ((DX != 1) && (DY != 1)) {
                return MOVE_ERROR_ILLEGAL;
            }
            if ((DX == 1) && (DY == 1) && TEMP == nullptr) {
                return MOVE_ERROR_ILLEGAL;
            }
            if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
                return MOVE_CAPTURE;
            }
        }

    }
    return SUCCESS;
}

int Rook::validMove(Position start, Position end, const Board& board) const {
    int b = Piece::validMove(start,end,board);
    if (b != SUCCESS) {
        return b;
    }
    int ret = Piece::validStraight(start,end,board);
    return ret;
    }
int Bishop::validMove(Position start, Position end, const Board& board) const {
    int b = Piece::validMove(start,end,board);
    if (b != SUCCESS) {
	return b;
    }
    int ret = Piece::validDiag(start,end,board);
    return ret; 
    }

int Queen::validMove(Position start, Position end, const Board& board) const {
    int b = Piece::validMove(start,end,board);
    if (b != SUCCESS) {
	return b;
    }
    int dx = end.x - start.x;
    int dy = end.y - start.y;
    int DX = sqrt(dx*dx);
    int DY = sqrt(dy*dy);

    if (DX == DY) { // like Bishop
        int ret1 = Piece::validDiag(start,end,board);
	return ret1;
    }
    else {  // like Rook, or invalid, but handled by the function as error
        int ret2 = Piece::validStraight(start,end,board);
	return ret2;
    } 
}
int Knight::validMove(Position start, Position end, const Board& board) const {
    int b = Piece::validMove(start,end,board);
    if (b != SUCCESS) {
	return b;
    }
    int DX = end.x - start.x;
    int DY = end.y - start.y;
    int dx = sqrt(DX*DX);
    int dy = sqrt(DY*DY);

    Piece * temp = board.getPiece(start); // look at piece at start positiom
    Piece * TEMP = board.getPiece(end); // look at piece at end position
    int OWNER = temp->owner();
    int ENEMY;
    if (OWNER == 1) {
        ENEMY = 0;
    }
    else {
        ENEMY = 1;
    }

    if ((dy == 0) && (dx != 0)) {
        return MOVE_ERROR_ILLEGAL;
    }
    if ((dx == 0) && (dy != 0)) {
        return MOVE_ERROR_ILLEGAL;
    }
    if (dx == dy) {
        return MOVE_ERROR_ILLEGAL;
    }

    if (DY > 0) { // FORWARD MOVE
        if (DX > 0) { // NORTH-EAST
            if (dy > dx) {
                if ((dy != 2) || (dx != 1)) { // if not upside-down 'L'
		    return MOVE_ERROR_ILLEGAL;
                }
		if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
		    return MOVE_CAPTURE;
		}
		if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
            else if (dx > dy) {
                if ((dy != 1) || (dx != 2)) { // if not lying-down 'L'
		    return MOVE_ERROR_ILLEGAL;
                }
		if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
		    return MOVE_CAPTURE;
		}
		if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
        }
        else if (DX < 0) { // NORTH-WEST
            if (dy > dx) {
                if ((dy != 2) || (dx != 1)) {
                    return MOVE_ERROR_ILLEGAL;
                }
		if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
                    return MOVE_CAPTURE;
                }
                if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
            else if (dy < dx) {
                if ((dy != 1) || (dx != 2)) {
                    return MOVE_ERROR_ILLEGAL;
                }
	        if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
                    return MOVE_CAPTURE;
                }
                if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
        }
    }
    else if (DY < 0) { // BACKWARD MOVE
        if (DX > 0) { // SOUTH-EAST
            if (dy > dx) {
                if ((dy != 2) || (dx != 1)) {
                    return MOVE_ERROR_ILLEGAL;
                }
		if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
                    return MOVE_CAPTURE;
                }
                if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
            else if (dy < dx) {
                if ((dy != 1) || (dx != 2)) {
                    return MOVE_ERROR_ILLEGAL;
                }
		if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
                    return MOVE_CAPTURE;
                }
                if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
        }
        else if (DX < 0) { // SOUTH-WEST
            if (dy > dx) {
                if ((dy != 2) || (dx != 1)) {
                    return MOVE_ERROR_ILLEGAL;
                }
		if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
                    return MOVE_CAPTURE;
                }
                if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
            else if (dy < dx) {
                if ((dy != 1) || (dx != 2)) {
                    return MOVE_ERROR_ILLEGAL;
                }
		if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
                    return MOVE_CAPTURE;
                }
                if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
        }
    }
    return SUCCESS;
}
int King::validMove(Position start, Position end,const Board& board) const {
    int b = Piece::validMove(start,end,board);
    if (b != SUCCESS) {
	return b;
    }
    int DX = end.x - start.x;
    int DY = end.y - start.y;
    int dx = sqrt(DX*DX);
    int dy = sqrt(DY*DY);

    Piece * temp = board.getPiece(start); // look at piece at start position
    Piece * TEMP = board.getPiece(end); // look at piece at end position
    int OWNER = temp->owner();
    int ENEMY;
    if (OWNER == 1) {
        ENEMY = 0;
    }
    else {
        ENEMY = 1;
    }

    if (DX == 0) { // move vertical
        if (dy != 1) {
	    return MOVE_ERROR_ILLEGAL;
	}
	if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {
	    return MOVE_ERROR_BLOCKED;
	}
	if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
	    return MOVE_CAPTURE;
	}
    }
    else if (DY == 0) { // move horizontal
        if (dx != 1) {
	    return MOVE_ERROR_ILLEGAL;
	}
	if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {
            return MOVE_ERROR_BLOCKED;
        }
        if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
            return MOVE_CAPTURE;
        }
    }
    else if (dx == dy) { // move diagonal
         if ((dx != 1) && (dy != 1)) {
	     return MOVE_ERROR_ILLEGAL;
	 }
	 if ((TEMP != nullptr) && (TEMP->owner() == OWNER)) {
             return MOVE_ERROR_BLOCKED;
         }
         if ((TEMP != nullptr) && (TEMP->owner() == ENEMY)) {
             return MOVE_CAPTURE;
         }
    }
    else { // if not above, invalid
        return MOVE_ERROR_ILLEGAL;
    }
    return SUCCESS;
}
