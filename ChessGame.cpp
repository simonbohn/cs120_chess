
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"
#include "Piece.h"
#include "ChessGame.h"

// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end) {
    // Possibly implement chess-specific move logic here
    //
    // We call Board's makeMove to handle any general move logic
    // Feel free to use this or change it as you see fit
    

/*
    Piece * start_loc_piece = getPiece(start);
    Piece * end_loc_piece = getPiece(end);
    unsigned int temp = 0;
    temp = start_loc_piece->id();
    start_loc_piece = nullptr;
*/
    int retCode = Board::makeMove(start, end);
    return retCode;

    }


// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
   //initPiece(ROOK_ENUM, WHITE, Position(5,5)); 
}


//Sweeps board for eligible pawns to promote (white on rank 8 and black on rank 0). If it is eligible, deletes the pawn and places a queen there.
void ChessGame::pawnPromotion(){
    for(int i = 0; i < 7; ++i){
                    Piece * temp  = getPiece(Position(i,0));
                    if(temp != nullptr){
                    if(temp->id() == 0){
                            delete(getPiece(Position(i,0)));
                            //initPiece requires a null ptr work on...smh
                            m_pieces[index(Position(i,0))] = nullptr;
                            initPiece(QUEEN_ENUM,BLACK,Position(i,0));
                    }
                    }
                    Piece * temp2 = getPiece(Position(i,7));
                    if(temp2 != nullptr){
                    if(temp2->id() == 0){
                            delete(getPiece(Position(i,7)));
                            m_pieces[index(Position(i,7))] = nullptr;
                            initPiece(QUEEN_ENUM,WHITE,Position(i,7));
                    }
                    }
    }

}


//Loads game from file. Parses it and initializes a board with the specified pieces. Only validation here is that the file says "chess".
int ChessGame::loadGame(string inName){
        int pieceOwner;
        char ColRaw;
        int RowRaw;
        int pieceID;
        string temp;
        std::ifstream inFile;
        inFile.open(inName);
        std::getline (inFile,temp);//Eats first line
        if(temp != "chess"){
            return -1;
        }
        std::getline(inFile,temp);//gets the next line and sets it to the current turn number when I get around to it
        m_turn = std::stoi(temp);
        while(std::getline(inFile,temp)){
            std::sscanf(temp.c_str(),"%1d %1c%1d %1d",&pieceOwner,&ColRaw,&RowRaw,&pieceID);
            if(pieceOwner == WHITE){
            initPiece(pieceID,WHITE,Position(letterToNumber(ColRaw),(RowRaw-1)));
            }
            else if(pieceOwner == BLACK){
            initPiece(pieceID,BLACK,Position(letterToNumber(ColRaw),(RowRaw-1)));
            }
        }
        inFile.close();
        return 0;

}

//I chose blue and yellow instead of white and black because 
//the default colors are different for my partner and I. 
//He likes to work black-on-white and I like to workwhite-on-black. -SIMON
//
//I added some small detail, like the piece names. -NATHAN
void ChessGame::printBoard(){
   for (int j = 7; j>=0;--j){//Must print backwards so that board orientation is black on top
    for (int i = 0; i< 8; ++i){
        Piece * temp  = getPiece(Position(i,j)); 
        if(temp != NULL) {
                if(temp->owner() == 1){ //If player is black, print it in blue
                    Terminal::colorFg(false,Terminal::BLUE);
                    if (temp->id() == 0) {
		        std::cout << "P ";
		    }
		    else if (temp->id() == 1) {
		        std::cout << "R ";
		    }
		    else if (temp->id() == 2) {
		        std::cout << "N ";
		    }
		    else if (temp->id() == 3) {
		        std::cout << "B ";
		    }
		    else if (temp->id() == 4) {
		        std::cout << "Q ";
		    }
		    else if (temp->id() == 5) {
		        std::cout << "K ";
		    }
		    //else {
		    //std::cout << temp->id();
		    //}
                }
                else{//Otherwise print in yellow
                    Terminal::colorFg(false,Terminal::YELLOW);
                     if (temp->id() == 0) {
                        std::cout << "P ";
                    }
                    else if (temp->id() == 1) {
                        std::cout << "R ";
                    }
                    else if (temp->id() == 2) {
                        std::cout << "N ";
                    }
                    else if (temp->id() == 3) {
                        std::cout << "B ";
                    }
                    else if (temp->id() == 4) {
                        std::cout << "Q ";
                    }
                    else if (temp->id() == 5) {
                        std::cout << "K ";
                    }
		    //cout<<temp->id();
                }
        }
        else{//Print empty spaces in terminal default color
                Terminal::colorFg(false,Terminal::DEFAULT_COLOR);
                std::cout<<"* ";
        }
    }
    cout<<std::endl;
   }
   Terminal::colorFg(false,Terminal::DEFAULT_COLOR);//Resets terminal color
}


//Function is used to convert index numbers to letters for use in savegame function.
char ChessGame::columnNumToLetter(int n){
    if(n == 0){
        return 'a';
    }
    else if(n == 1){
        return 'b';
    }
    else if(n == 2){
        return 'c';
    }
    else if(n == 3){
        return 'd';
    }
    else if(n ==4){
        return 'e';
    }
    else if(n == 5){
        return 'f';
    }
    else if(n == 6){
        return 'g';
    }
    else if(n == 7){
        return 'h';
    }
    return 'z';
}

//Function saves game by looping through board and where there are pieces, printing their location and type to the save file.
void ChessGame::saveGame(string outName){
        std::ofstream saveFile;
        saveFile.open(outName);
        saveFile<<"chess"<<std::endl;
        saveFile<<turn()<<std::endl;
        for(int i = 0; i < 8; ++i){
                for(int j = 0; j < 8; ++j){
                        Piece * temp  = getPiece(Position(i,j));
                        if(temp != NULL){
                                saveFile<<temp->owner()<<" "<<columnNumToLetter(i)<<(j+1)<<" "<<temp->id()<<std::endl;
                        }
                }
        }
        saveFile.close();
}
