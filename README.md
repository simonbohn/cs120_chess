Hello and welcome to Nathan and Simon's Chess Emporium.


TEAM: Nathan Bick (nbick1) and Simon Bohn (sbohn5)

DESIGN: Most major classes have their own .cpp files, with exception of the stuff that began in Game.h and we did not feel comfortable re-factoring because of the strict requirements to maintain the skeleton. "Piece" and its sublcasses were moved to their own file, and "ChessGame" was as well. The Chess file now only contains the main{} function of the game. 

The game initialization takes place in that main{} function. After that, the run method of a chessGame object is called. 

Within the run() method, the gameplay loop plays out. This prompts for options and accepts valid input. For valid move-makign input, the input is validate. To  validate the input, the makeMove() function of the board is called. This then looks at the piece, calls any specific movement issues (e.g. moving a piece that doesn't exist), and calls the master validMove method of the selected piece, which looks for movement issues (e.g. rook moving diagonally). Finally, validMove of a specific piece calls the generic one for Piece, which looks for things that should not be legal for any piece, like moving onto a square where a piece already is (we assume the mystery piece cannot do this). At least I think that's how it works. My partner was responsible for that full stack of functions and I took care of the rest of the stuff including this readme.



One design niceity here is that because we realized rook and bishop are subsets of the queen, we reuse the validation methods and call them as part of Piece. Rook calls the one about moving laterally, bishop the one about moving diagonally, queen calls both.

Also, we know that some of the stuff we implemented in Board should be in ChessGame, as they are chess specific but it was initially done in Board because it was quick and easy to debug with run() so close. We didn't want to move it because it was working.

Finally, if the move is valid, it goes through a number of functions to check if it puts the player in check (if it does, the move is taken back), and a function to check for checkmate.

The turn is finished off by sweeping the board for pawns at the last rank and promoting them, and checking for stalemate.

COMPLETENESS: 


We did not implement castleing. Error messages in cases of check and checkmate are not prioritized. Because this is so minor I consider this to be passing end-to-end tests.

Stalemate does not work. We wrote it and it fails its unittest; we believe this is because the validmove function is failing in the specific setup we gave it. It appears to be working everywhere else, though, so we're not really sure what to make of it. It is commented out of run() in Board.cpp, but the code for it is all there and it appears in a unittest. Cheers!


SPECIAL INSTRUCTIONS:
To our knowledge there are no really major issues besides stalemate.
I'm not sure we implemented the latest change to Prompts.txt with the extra new line.



TESTING EXPLANATION:



Unit Test Explanation: Unit tests are explained in the unit testing file. Most of them load a default board, but a few load saved games as do the end-to-end tests. They are explained below:

foolsmate.txt - Has a valid move available for the left, white rook and for the right black bishop, then pawns in the way for their subsequent moves to test their piece-pass-through logic
intocheck.txt - A one move check is available and it should not allow white to move into it

End-To-End testing explanation:

No unit tests are done on save and load game because they are tested in end-to-end tests.

End-to-end testing script is called test.sh and requires a #of tests argument e.g. ./test.sh 9

-in1.txt tests starting a new game, making a default board then quitting
-in2.txt tests starting a new game, making a default board (but messing up on the way) and then forfeiting
-in3.txt tests starting a new game, making some moves that are invalid for all the right reasons and then forfeiting
-in4.txt tests loading a game and moving a piece that puts the game into checkmate, and ends the game with the proper winner.
-in5.txt tests loading a game, moving a piece that puts the player into checkmate and then disallowing said move, then quitting.
-in6.txt tests loading a game where a pawn is eligible to be promoted and then promotes it. This one also tests capturing pieces.
-in7.txt tests starting a new game, and then saving it
-in8.txt tests loading an invalid file
-in9.txt tests parse error input


