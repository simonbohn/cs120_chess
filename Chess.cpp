#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"
#include "Piece.h"
#include "ChessGame.h"

using std::cout;


int main() {
    
    //Initializes a chess game!    
    ChessGame chess;
    int initDone = 0;
    string initInput;
    string loadgameInput;
    int s = 0;
    while(initDone == 0){
        Prompts::menu();
        std::getline(cin,initInput);
        if(initInput == "1"){
            chess.setupBoard();
            break;

        }
        else if(initInput == "2"){
            Prompts::loadGame();
            std::getline(cin,loadgameInput);
            s = chess.loadGame(loadgameInput);
            if(s != 0){
                Prompts::loadFailure();
                return -1;
            }
            break;
        } 
    }
    //Runs the initialized chess game
    chess.run();
    return 0;
}
