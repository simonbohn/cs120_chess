#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"
#include "Piece.h"
#include "ChessGame.h"

#include <iostream>
#include "unittest.h"
using std::cout;
using std::endl;


//Note: These functions assume the save and load functions are functional. This is tested in the end-to-end tests.
//This function tests for a piece's generic validity. These are moves that invalid for ALL pieces and include the following: moving a piece into a space occupied by one of the player's own pieces, moving a piece that is not a knight through other pieces
int GenericPieceValidationTest(){
    cout<<endl;
    ChessGame genericTestGame;
    genericTestGame.setupBoard();
    //genericTestGame.printBoard();
    int t = 0; //t is test results
    int f = 0; //f is tests failed
    //Moving a piece into one of your own pieces
    t = genericTestGame.makeMove(Position(0,1),Position(1,1));
    if(t != 0){cout<<"Test failed where piece is moved into your own piece"<<endl;f++;}
    //Moving a piece THROUGH one of your own pieces, when that piece is not at the destination
    t = genericTestGame.makeMove(Position(0,0),Position(0,5));
    if(t!= 0){cout<<"Test failed where piece is moved through your own piece"<<endl;f++;}
    //Moving a piece off the board
    t = genericTestGame.makeMove(Position(0,0),Position(100,0));
    if(t!= 0){cout<<"Test failed where piece is moved off the board"<<endl;f++;}
    cout<<"Of the three generic moves tested, "<<f<<" failed unittests"<<endl;
   // genericTestGame.printBoard();
    return f;
}


int CheckAndMateValidationTest(){
        int t = 0;
        int f = 0;
        ChessGame mateGame;
        mateGame.loadGame("foolsmate.txt"); //Loads a game that is one move away from the "fool's mate". Wikipedia has a good article on it.
        
     t = mateGame.checkCheckmate(mateGame.playerTurn());
     if(t == 1){cout<<"FAILED UNITTEST: CheckCheckmate is not firing"<<endl;f++;}
        
    
        ChessGame checkGame;
        //checkGame.setupBoard();
        //Generates a game with only two pieces on the board: a white king and a black pawn. The king is in check, but not checkmate.
        checkGame.initPiece(KING_ENUM,BLACK,Position(0,0));
        checkGame.initPiece(ROOK_ENUM,WHITE,Position(0,1));
        t = 56;//just making sure it's all clear
        t = checkGame.checkCheck(checkGame.playerTurn());
        if(t == 0){
                cout<<"FAILED UNITTEST: checkCheck function is failing to return 1 when in check. Instead it is returning: "<<t<<endl;
                f++;
        }
        cout<<endl;
        
        
        
        ChessGame intoCheckGame;
        intoCheckGame.loadGame("intoCheck");
        //intoCheckGame.printBoard();
        t = intoCheckGame.validMoveCheck(Position(4,0),Position(4,1));
        //intoCheckGame.printBoard();
        if(t == 1){cout<<"FAILED UNITTEST: Piece is being allowed to move into position that exposes check";f++;}
        //Makes a stalemate board
        ChessGame staleGame;
        staleGame.initPiece(KING_ENUM,WHITE,Position(4,7));
        staleGame.initPiece(PAWN_ENUM,BLACK,Position(4,6));
        staleGame.initPiece(KING_ENUM,BLACK,Position(4,5));
        //staleGame.printBoard();
        t = staleGame.checkStale(staleGame.playerTurn());
        if(t == 0){cout<<"FAILED UNITTEST: Stalemate is not firing when a stalemate exists";f++;} 
        


        return f;
}



//This function tests the validity of functions specific to the pawn. The pawn should be able to move two forward, only on their first move
//

int testPawnValidation(){
    int t = 0;
    int f = 0;
    ChessGame pawnTestGame;
    pawnTestGame.setupBoard();
    //Test if pawn can make double move on first turn
    t = pawnTestGame.makeMove(Position(3,1),Position(3,3));
    if(t != 1){cout<<"FAILED UNITTEST: Pawn could not make a double move on first turn"<<endl;f++;} 
    //Test if pawn cannot make double move on subsequent turns
    t = pawnTestGame.makeMove(Position(3,3),Position(3,5));
    if(t == 1){cout<<"FAILED UNITTEST: Pawn was able to move two spaces in a non-first move"<<endl;f++;}
    //Test if pawn CANNOT move diagonally normally
    t =  pawnTestGame.makeMove(Position(3,3),Position(4,4));
    if(t != 0){cout<<"FAILED UNITTEST: Pawn was able to move diagonally when not capturing"<<endl;f++;}
    
    //Initializes a queen for the pawn to capture
    pawnTestGame.initPiece(QUEEN_ENUM,BLACK,Position(4,4));
    t = pawnTestGame.makeMove(Position(3,3),Position(4,4));
    if (t != 1){cout<<"FAILED UNITTEST: Pawn was not able to capture a piece diagonally"<<endl;f++;}
    //Test if pawn cannot move backwards normally
    t = pawnTestGame.makeMove(Position(4,4),Position(4,3));
    if (t != 0){cout<<"FAILED UNITTEST: Pawn was able to move backwards"<<endl;f++;}
   
    //Test if pawn cannot move sideways
    t = pawnTestGame.makeMove(Position(4,4),Position(3,4));
    if(t == 1){cout<<"FAILED UNITTEST: Pawn was able to move sideways"<<endl;f++;}

//    pawnTestGame.printBoard();
    return f;
    
}

//Tests queen and king validation

int testQueenAndKingValidation(){

    //Makes sure queen cannot jump diagonals
    int t = 0;
    int f = 0;
    ChessGame queenTestGame;
    queenTestGame.initPiece(QUEEN_ENUM,WHITE,Position(4,4));

    t = queenTestGame.makeMove(Position(4,4),Position(4,6));
    if(t != 1){cout<<"FAILED UNITTEST: Queen was not able to move like a rook"<<endl;f++;}
    t = queenTestGame.makeMove(Position(4,6),Position(5,7));
    if(t != 1){cout<<"FAILED UNITTEST: Queen was not able to move like bishop"<<endl;f++;}
    t = queenTestGame.makeMove(Position(5,7),Position(4,4));
    if (t != 0){cout<<"FAILED UNITTEST: Queen was able to make an illegal move"<<endl;f++;}

    ChessGame kingTestGame;
    kingTestGame.initPiece(KING_ENUM,WHITE,Position(4,4));
    
    t = kingTestGame.makeMove(Position(4,4),Position(5,5));
    if(t != 1){cout<<"FAILED UNITTEST: King was not able to move diagonally"<<endl;f++;}
    t = kingTestGame.makeMove(Position(5,5),Position(4,5));
    if(t != 1){cout<<"FAILED UNITTEST: King was not able to move vertically"<<endl;f++;}
    t = kingTestGame.makeMove(Position(4,5),Position(4,7));
    if(t != 0){cout<<"FAILED UNITTEST: King was allowed to move multiple spaces"<<endl;f++;}

    return f;

    
}




//This tests rooks and bishops. They are tested together because they're both subsets of the queen.
int testRookAndBishopValidation(){
    int t= 0;
    int f = 0;
    ChessGame RookTestGame;
    cout<<"Testing Rook"<<endl;
    RookTestGame.initPiece(ROOK_ENUM,WHITE,Position(4,4));
    t = RookTestGame.makeMove(Position(4,4),Position(5,5));
    if(t == 1){cout<<"FAILED UNITTEST: Rook was able to move diagonally"<<endl;f++;}
    t = RookTestGame.makeMove(Position(4,4),Position(4,6));
    if(t != 1){cout<<"FAILED UNITTEST: Rook was not able to move laterally"<<endl;f++;}
    RookTestGame.initPiece(PAWN_ENUM,WHITE,Position(4,5));
    t = RookTestGame.makeMove(Position(4,6),Position(4,4));
    if (t == 1){cout<<"FAILED UNITTEST: Rook was able to move through a piece"<<endl;f++;}


    ChessGame BishopTestGame;
    BishopTestGame.initPiece(BISHOP_ENUM,WHITE,Position(4,4));
    cout<<"Testing bishop"<<endl;
    t = BishopTestGame.makeMove(Position(4,4),Position(6,6));
    if(t != 1){cout<<"FAILED UNITTEST: Bishop was not able to move diagonally"<<endl;f++;}
    t = BishopTestGame.makeMove(Position(6,6),Position(6,7));
    if(t == 1){cout<<"FAILED UNITTEST: Bishop was  able to move vertically"<<endl;f++;}
    BishopTestGame.initPiece(PAWN_ENUM,WHITE,Position(5,5));
    t = BishopTestGame.makeMove(Position(6,6),Position(4,4));
    if (t == 1){cout<<"FAILED UNITTEST: Bishop was able to move through a piece"<<endl;f++;}
    
    return f;



}

int testKnight(){
    int t = 0;
    int f = 0;
    ChessGame testKnight;
    testKnight.setupBoard();
    t = testKnight.makeMove(Position(1,0),Position(2,2));
    if(t != 1){cout<<"FAILED UNITTEST: Knight was not allowed to make a legal move (and move through a piece)"<<endl;f++;}
    t = testKnight.makeMove(Position(2,2),Position(3,3));
    if (t == 1){cout<<"FAILED UNITTEST: Knight was allowed to make an illegal move (move by one space)"<<endl;++f;}
return f;
}

int main(){
int f = 0;
        cout<<"Note: This program will print a few error messages as it simulates illegal moves being played. they are not unit test failures. The real unit test failures come with a message that says FAILED UNITTEST and explain why"<<endl;    
        f+=GenericPieceValidationTest();
        f+=CheckAndMateValidationTest();
        f+=testPawnValidation();
        f+=testQueenAndKingValidation();
        f+=testRookAndBishopValidation();
        f+=testKnight();
        cout<<endl<<"Unittests failed: "<<f<<endl;
        return 1;
    
}
